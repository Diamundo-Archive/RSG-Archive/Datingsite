<?php

$error_voornaam_empty = "Je hebt je <u>Voornaam</u> niet ingevuld!<br>";
$error_lastname_empty = "Je hebt je <u>Achternaam</u> niet ingevuld!<br>";
$error_gender_empty = "Je hebt je <u>Geslacht</u> niet ingevuld!<br>";
$error_prefsex_empty = "Je hebt je <u>Voorkeur</u> niet ingevuld!<br>";
$error_province_empty = "Je hebt je <u>Provincie</u> niet ingevuld!<br>";
$error_religion_empty = "Je hebt je <u>Religie</u> niet ingevuld!<br>";
$error_school_empty = "Je hebt je <u>Opleiding</u> niet ingevuld!<br>";
$error_skin_empty = "Je hebt je <u>Huidskleur</u> niet ingevuld!<br>";
$error_hair_empty = "Je hebt je <u>Haarkleur</u> niet ingevuld!<br>";
$error_eye_empty = "Je hebt je <u>Oogkleur</u> niet ingevuld!<br>";
$error_smoke_empty = "Je hebt niet ingevuld of je <u>Rookt</u>!<br>";
$error_kids_empty = "Je hebt je <u>aantal Kinderen</u> niet ingevuld!<br>";
$error_wish_empty = "Je hebt je <u>Kinderwens</u> niet ingevuld!<br>";

$lang = array();
$lang['filling'] = "Vul je profiel in";
$lang['firstname'] = "Wat is je Voornaam?";
$lang['lastname'] = "Wat is je Achternaam?";
$lang['gender'] = "Wat is je Geslacht?";
$lang['prefer'] = "Welk geslacht heeft jouw Voorkeur?";
$lang['province'] = "In welke Provincie woon je?";
$lang['religion'] = "Wat is je Religie?";
$lang['school'] = "Wat is je Opleiding?";
$lang['skin'] = "Wat is je Huidskleur?";
$lang['hair'] = "Wat is je Haarkleur?";
$lang['eye'] = "Wat is je Oogkleur?";
$lang['smoke'] = "Rook je?";
$lang['kids'] = "Hoeveel Kinderen heb je nu?";
$lang['wish'] = "Hoeveel Kinderen wil je nog?";
$lang['send'] = "Verstuur";
?>