<?php

$profbericht = "Has not yet entered a message.";
$titel = "Profile of ";
$profileof = "Profile of: ";
$persmsg = "Personal message: ";
$beroep = "Job: ";
$leeftijd = "Age: ";
$jaar = " years";
$email = "E-mail address";

$info = "Information about me:";
$prefersex = "I am attracted to the gender: ";
$prov = "I live in the province: ";
$haarkl = "The colour of my hair is: ";
$oogkl = "The colour of my eyes is: ";
$huidskl = "The colour of my skin is: ";
$geloof = "My religion is: ";
$educ = "My level of education is: ";

$mailen = "Do you wish to mail us? You can! Click ";
$mailen2 = "here!";

$curkids = array();
$curkids[0] = "I currently have";
$curkids[1] = " child";
$curkids[2] = "ren";

$kidwish = array();
$kidwish[0] = "I would like to have";
$kidwish[1] = " more child";
$kidwish[2] = "ren";
$kidwish[3] = "";

$jarig = array();
$jarig[0] = "<B>--> It's ";
$jarig[1] = "his";
$jarig[2] = "her";
$jarig[3] = " birthday today! Say 'Happy Birthday' to ";
$jarig[4] = "him";
$jarig[5] = "her";
$jarig[6] = "!<b>";

?>