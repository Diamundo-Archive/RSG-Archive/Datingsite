<?php
include_once('config.php');

if( !isset( $_GET['lang'])) { 
	include_once('lang/infoNL.php'); 
	include_once('lang/arrayNL.php'); 	
} else {
	switch( $_GET['lang'] ) {
		case "en" : 
			include_once('lang/infoEN.php'); 
			include_once('lang/arrayEN.php');
			break;
		case "nl" : 
		default : 
			include_once('lang/infoNL.php'); 
			include_once('lang/arrayNL.php');
			break;
	}
}

if(!isset($_SESSION)){ session_start(); }

if(!isset($_SESSION['username']))
  	{ header('Location: '.$homepage.' '); }

if( isset($_POST['controle']) AND $_POST['controle']=="TRUE") {
	$con = openConnection();

	$error = false; 
	IF (
		   (empty($_POST['voornaam']))
		OR (empty($_POST['achternaam']))
		OR ($_POST['geslacht'] == 0)
		OR ($_POST['prefsex'] == 0)
		OR ($_POST['provincie']==0)
		OR ($_POST['religie']==0)
		OR ($_POST['opleiding']==0)
		OR ($_POST['huidskleur']==0)
		OR ($_POST['haarkleur']==0)
		OR ($_POST['oogkleur']==0)
		OR ($_POST['roken']==0)
		OR ($_POST['kinderen']==0)
		OR ($_POST['kinderwens']==0)
	) { 
		$error_head="<b>!! ERROR !!</b>";
		$error=true; 
	}
			
	IF ( $error ) {
		IF (empty($_POST['voornaam']))
			{ $error_voornaam=$error_voornaam_empty; }
		IF (empty($_POST['achternaam']))
			{ $error_achternaam=$error_lastname_empty; }
		IF (empty($_POST['geslacht']))
			{ $error_geslacht=$error_gender_empty; }
		IF (empty($_POST['prefsex']))
			{ $error_prefsex=$error_prefsex_empty; }
		IF ($_POST['provincie']==0)
			{ $error_provincie=$error_province_empty; }
		IF ($_POST['religie']==0)
			{ $error_religie=$error_religion_empty; }
		IF ($_POST['opleiding']==0)
			{ $error_opleiding=$error_school_empty; }
		IF ($_POST['huidskleur']==0)
			{ $error_huidskleur=$error_skin_empty; }
		IF ($_POST['haarkleur']==0)
			{ $error_haarkleur=$error_hair_empty; }
		IF ($_POST['oogkleur']==0)
			{ $error_oogkleur=$error_eye_empty; }
		IF ($_POST['roken']==0)
			{ $error_roken=$error_smoke_empty; }
		IF ($_POST['kinderen']==0)
			{ $error_kinderen=$error_kids_empty; }
		IF ($_POST['kinderwens']==0)
			{ $error_kinderwens=$error_wish_empty; }
	} else {
		$sql_user=( " UPDATE user
			SET Firstname='".mysql_real_escape_string($_POST['voornaam'])."', Lastname='".mysql_real_escape_string($_POST['achternaam'])."', Gender='".$_POST['geslacht']."', 
				prefsex='".$_POST['prefsex']."', provincie='".$_POST['provincie']."', religie='".$_POST['religie']."', opleiding='".$_POST['opleiding']."', 
				huidskleur='".$_POST['huidskleur']."', haarkleur='".$_POST['haarkleur']."', oogkleur='".$_POST['oogkleur']."', roken='".$_POST['roken']."', 
				kinderen='".$_POST['kinderen']."', kinderwens='".$_POST['kinderwens']."', picture='".$_POST['geslacht'].".png'
			WHERE username='".$_SESSION['username']."' ");

		IF (!mysql_query($sql_user, $con)) { 
			die ("Query Error 1: " . mysql_error()); 
		}
			
		header("Location: editprofile.php");

	} 
	mysql_close($con);
} 
?>

<html>
<head>
<title><?php echo $lang['filling']; ?> - <?php echo $sitename; ?></title>
<link rel="icon" type="image/ico" href="/dating/favicon.ico"> </link>
</head>
<body bgcolor="#00BFFF">
<?php include('menu.php'); ?>
<hr>
<font color="#FF0000"> <?php if(isset($error_head)){echo $error_head;}?></font><br />

<table>
<form action="" method="post">

<tr> <td width="250"><?php echo $lang['firstname']; ?></td>
<td width="100"><input type="text" name="voornaam" size="30" value="<?php if(isset($_POST['voornaam'])){echo $_POST['voornaam'];} ?>"></td>
<td width="10"></td> <td width="600"><font color="#FF0000"> <?php if(isset($error_voornaam)){echo $error_voornaam; }?> </font> </td></tr>

<tr><td><?php echo $lang['lastname']; ?></td>
<td><input type="text" name="achternaam" size="30" value="<?php if(isset($_POST['achternaam'])){echo $_POST['achternaam'];} ?>"></td>
<td></td> <td><font color="#FF0000"> <?php if(isset($error_achternaam)){echo $error_achternaam;} ?> </font> </td></tr>

<tr><td><?php echo $lang['gender']; ?></td><td>
<?php
echo '<select name="geslacht">';
for($i=0;$i<3;$i++){
   if($i ==$_POST["geslacht"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$gender[$i].'</option>'; }
echo '</select>';
?></td>
<td></td> <td><font color="#FF0000"> <?php if(isset($error_geslacht)){echo $error_geslacht;} ?> </font> </td></tr>

<tr><td><?php echo $lang['prefer']; ?></td>
<td><?php
echo '<select name="prefsex">';
for($i=0;$i<4;$i++){
   if($i ==$_POST["prefsex"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$prefer[$i].'</option>'; }
echo '</select>';
?></td>
<td></td> <td><font color="#FF0000"> <?php if(isset($error_prefsex)){echo $error_prefsex;} ?> </font> </td></tr>


<tr><td><span><?php echo $lang['province']; ?></span></td>
<td><?php
echo '<select name="provincie">';
for($i=0;$i<13;$i++){
   if($i ==$_POST["provincie"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$province[$i].'</option>'; }
echo '</select>';
?></td>

<td></td> <td><font color="#FF0000">  <?php if(isset($error_provincie)){echo $error_provincie;} ?> </font> </td></tr>

<tr><td><span><?php echo $lang['religion']; ?></span></td>
<td><?php
echo '<select name="religie">';
for($i=0;$i<11;$i++){
   if($i ==$_POST["religie"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$religion[$i].'</option>'; }
echo '</select>';
?></td>
<td></td> <td><font color="#FF0000"> <?php if(isset($error_religie)){echo $error_religie;} ?> </font> </td></tr>	

<tr><td><span><?php echo $lang['school']; ?></span></td>
<td><?php
echo '<select name="opleiding">';
for($i=0;$i<8;$i++){
   if($i ==$_POST["opleiding"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$school[$i].'</option>'; }
echo '</select>';
?></td>
<td></td> <td><font color="#FF0000"> <?php if(isset($error_opleiding)){echo $error_opleiding;} ?> </font> </td></tr>

<tr><td><span><?php echo $lang['skin']; ?></span></td>
<td><?php
echo '<select name="huidskleur">';
for($i=0;$i<7;$i++){
   if($i ==$_POST["huidskleur"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$skin[$i].'</option>'; }
echo '</select>';
?></td>

<td></td> <td> <font color="#FF0000"> <?php if(isset($error_huidskleur)){echo $error_huidskleur;} ?> </font> </td></tr>

<tr><td><span><?php echo $lang['hair']; ?></span></td>
<td><?php
echo '<select name="haarkleur">';
for($i=0;$i<11;$i++){
   if($i ==$_POST["haarkleur"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$hair[$i].'</option>'; }
echo '</select>';
?></td>

<td></td> <td><font color="#FF0000">  <?php if(isset($error_haarkleur)){echo $error_haarkleur;} ?> </font> </td></tr>

<tr><td><span><?php echo $lang['eye']; ?></span></td>
<td><?php
echo '<select name="oogkleur">';
for($i=0;$i<5;$i++){
   if($i ==$_POST["oogkleur"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$eye[$i].'</option>'; }
echo '</select>';
?></td>

<td></td> <td> <font color="#FF0000"> <?php if(isset($error_oogkleur)){echo $error_oogkleur;} ?> </font> </td></tr>

<tr><td><?php echo $lang['smoke']; ?></td>
<td><?php
echo '<select name="roken">';
for($i=0;$i<3;$i++){
   if($i ==$_POST["roken"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$smoke[$i].'</option>'; }
echo '</select>';
?></td>

<td></td> <td><font color="#FF0000">  <?php if(isset($error_roken)){echo $error_roken;} ?> </font> </td></tr>

<tr><td><span><?php echo $lang['kids']; ?></span></td>
<td><?php
echo '<select name="kinderen">';
for($i=0;$i<7;$i++){
   if($i ==$_POST["kinderen"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$kids[$i].'</option>'; }
echo '</select>';
?></td>

	<td></td> <td> <font color="#FF0000"> <?php if(isset($error_kinderen)){echo $error_kinderen;} ?> </font> </td></tr>

<tr><td><span><?php echo $lang['wish']; ?></span></td>
<td><?php
echo '<select name="kinderwens">';
for($i=0;$i<7;$i++){
   if($i ==$_POST["kinderwens"]){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$i.'" '.$selected.'>'.$wish[$i].'</option>'; }
echo '</select>';
?></td>

	<td></td> <td> <font color="#FF0000"> <?php if(isset($error_kinderwens)){echo $error_kinderwens;} ?> </font> </td></tr>
	
</table><br />
<input type="hidden" name="controle" value="TRUE">
<input type="submit" value='<?php echo $lang['send']; ?>'>
</form>

<hr>
<form name="reset" method="post" action="">
<input type="submit" value="Reset"><br />
</form>
</body>
</html>
